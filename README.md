
# Swedish Pizza
![Python logo](http://kebabresan.se/wp-content/uploads/2013/07/7dd6868ce73111e2877022000a9f1278_7.jpg)

## AppSpotr Challenge

Send a GET request to [https://api2.appspotr.com/givemeachallenge](https://api2.appspotr.com/givemeachallenge) and solve the problem in your favourite language. 

### Getting Started 

This solution makes use of [Python 2.7.10](https://www.python.org/downloads/) and the [Requests Module](http://www.python-requests.org/en/latest/user/install/)

*Install Requests*

```bash
$ pip install requests
```

*Clone the SwedishPizza Repository*

```bash
$ git clone git@gitlab.com:pmakarov/swedish-pizza.git
$ cd swedish-pizza
```
*Execute the Script /swedish-pizza/kebabsas.py*

```bash
$ python kebabsas.py
```



## Discussion

**Python** was selected for this task for several reasons:

* Handy JSON module
* Easy API probing via the Requests Module
* Swedish colors


![Python logo](http://www.dexterindustries.com/wp-content/uploads/2013/04/python-logo.png )

Solution: Using a "hash map" seemed like the easiest way to solve this.  

![Script Screenshot](screenshot.png)

I had contemplated doing this in Javascript but knew that I could accomplish hashing and counting the occurences of the "quiz" items in a few lines in Python using the collections Counter class.  

![Script Screenshot](screenshot2.png)

Thanks for the fun challenge! I'm interested to learn how others solved this as well.



### Techology Used:
* Python 2.7.10
* Pip 7.0.3
* Sublime Text 3
* Pycharm 4
* MacDown v.05


