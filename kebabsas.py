# Objective: Given a list of duplicate words, 
# identify the "odd duck" case.
# Requirements: Solve this problem: 
# Send a GET request to https://api2.appspotr.com/givemeachallenge 
# and solve the problem in your favourite language

__author__ = "Paul Makarov"
__version__ = "0.1"
__email__ = "makarov9mm@gmail.com"
__status__ = "Development"

import requests, json
from datetime import datetime as dt
from collections import Counter

# The obligatory single-line solution:
# print Counter(json.loads(requests.get('https://api2.appspotr.com/givemeachallenge').text)["quiz"]).most_common()[-1][0]

# The solution:

# Log start of process
start = dt.now()

# Using the requests package, GET appSpotr API endpoint
appSpotrRequest = requests.get('https://api2.appspotr.com/givemeachallenge')

# Decode JSON 
decodedJSON = json.loads(appSpotrRequest.text)

# Use the Counter class to count "hash key" collisions
hashCounter = Counter(decodedJSON["quiz"])

# Order the given list by most common and target the final index for print.
# Since Python converted the original list to a list of Tuples means
# we have to select the Tuple index of 0 to see word rather than collision count
print hashCounter.most_common()[-1][0]

# Log end of process
end = dt.now()
elapsed = end - start

print 'Finished Hash Counting in ' + str(elapsed.total_seconds()) + ' seconds'
